const mysql = require('mysql2');

// Create a connection to the MySQL server
const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '1f2d1e2e67df',
  database: 'TEST',
});

// Connect to the database
connection.connect((err) => {
  if (err) {
    console.error('Error connecting to MySQL:', err);
    return;
  }

  console.log('Connected to MySQL');

  // Create the TEST table
  const createTableQuery = `
    CREATE TABLE IF NOT EXISTS TEST (
      id INT AUTO_INCREMENT PRIMARY KEY,
      name VARCHAR(255) NOT NULL,
      description TEXT
    )
  `;

  connection.query(createTableQuery, (createTableErr, results) => {
    if (createTableErr) {
      console.error('Error creating TEST table:', createTableErr);
    } else {
      console.log('TEST table created successfully');

      // Insert or update data in the TEST table
      const updateDataQuery = `
        INSERT INTO TEST (name, description) VALUES ('Example Name', 'Example Description')
        ON DUPLICATE KEY UPDATE description = 'Updated Description'
      `;

      connection.query(updateDataQuery, (updateDataErr, updateResults) => {
        if (updateDataErr) {
          console.error('Error updating data in TEST table:', updateDataErr);
        } else {
          console.log('Data inserted/updated in TEST table:', updateResults);
        }

        // Close the connection
        connection.end();
      });
    }
  });
});
