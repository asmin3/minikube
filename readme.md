1). Install Helm3.

Follow below official guide - https://helm.sh/docs/intro/install/

2). Install Promehtues with Helm.

helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm install prometheus prometheus-community/prometheus

3). Expose NodePort service for prometheus.

kubectl expose service prometheus-server --type=NodePort --target-port=9090 --name=prometheus-server-nodeport

4). Get URL for Prometheus.

minikube service prometheus-server-nodeport

5). Install Grafana with Helm.

helm repo add grafana https://grafana.github.io/helm-charts
helm install grafana grafana/grafana

6). Expose NodePort service for Grafana.

kubectl expose service grafana --type=NodePort --target-port=3000 --name=grafana-nodeport

7). Get URL for Grafana.
minikube service grafana-nodeport

8). Get dashboard password
kubectl get secret grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo